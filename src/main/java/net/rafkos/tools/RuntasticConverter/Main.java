package net.rafkos.tools.RuntasticConverter;

import java.io.File;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main
{
	public static void main(String[] args)
	{
		if(args.length != 2)
		{
			log.error("Please provide correct arguments:\n"
					+ "<Runtastic export folder> <Destination folder>");
			return;
		}
		
		final File runtasticExportDataFolder = new File(args[0]);
		final File convertedDataDestinationFolder = new File(args[1]);
		
		Converter.convertFromRuntasticToEndomondo(runtasticExportDataFolder, convertedDataDestinationFolder);
	}
}
