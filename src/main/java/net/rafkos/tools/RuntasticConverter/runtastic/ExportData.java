package net.rafkos.tools.RuntasticConverter.runtastic;

import java.io.File;
import java.io.FileReader;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import net.rafkos.tools.RuntasticConverter.Global;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExportData
{
	private long start_time, end_time, created_at, updated_at, start_time_timezone_offset, end_time_timezone_offset;
	private double distance;
	private int duration;
	private double elevation_gain, elevation_loss, average_speed, calories;
	private double longitude, latitude, max_speed;
	private long pause_duration, duration_per_km;
	private double temperature, pulse_avg, pulse_max, avg_cadence;
	private boolean manual, edited, completed, live_tracking_active, live_tracking_enabled, cheering_enabled, indoor;
	private String id;
	private int weather_condition_id, sport_type_id;
	
	@SneakyThrows
	public static ExportData fromFile(File file)
	{
		FileReader reader = new FileReader(file);
		ExportData ed = Global.JSON_PARSER.fromJson(reader, ExportData.class);
		reader.close();
		
		return ed;
	}
}
