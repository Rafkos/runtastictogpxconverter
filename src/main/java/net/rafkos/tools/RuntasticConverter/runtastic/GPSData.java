package net.rafkos.tools.RuntasticConverter.runtastic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rafkos.tools.RuntasticConverter.Global;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GPSData
{
	private int version;
	private String timestamp;
	private double longitude, latitude, altitude, accuracy_v, accuracy_h, speed;
	private int duration;
	private double distance, elevation_gain, elevation_loss;
	
	public static GPSData[] fromFile(File file)
	{
		log.info("Creating a GPS data from {}", file.getPath());
		
		FileReader reader = null;
		
		try
		{
			reader = new FileReader(file);
		}catch(FileNotFoundException e1)
		{
			log.error("GPS data not found {}", file.getPath());
			return null;
		}
		
		GPSData[] gpsDataArray = Global.JSON_PARSER.fromJson(reader, GPSData[].class);
		
		if(reader != null)
		{
			try
			{
				reader.close();
			}catch(IOException e)
			{
				log.error("An error has occured while closing IO reader {}", e.getLocalizedMessage());
			}
		}
		
		return gpsDataArray;
	}
}
