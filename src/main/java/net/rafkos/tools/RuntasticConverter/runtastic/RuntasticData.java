package net.rafkos.tools.RuntasticConverter.runtastic;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class RuntasticData
{
	public static final SimpleDateFormat TIMESTAMP_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z");
	public static final String DEFAULT_GPS_DATA_FOLDER = "GPS-data", DEFAULT_ELEVATION_DATA_FOLDER = "Elevation-data";

	@NonNull
	private ExportData exportData;
	@NonNull
	private ElevationData[] elevationDataArray;
	@NonNull
	private GPSData[] GPSDataArray;
	private String filename;
	
	public GPSData getFor(ElevationData elevationData)
	{
		for(GPSData d : GPSDataArray)
		{
			if(d.getTimestamp().equalsIgnoreCase(elevationData.getTimestamp()))
			{
				return d;
			}
		}
		
		return null;
	}
	
	public ElevationData getFor(GPSData gpsData)
	{
		for(ElevationData d : elevationDataArray)
		{
			if(d.getTimestamp().equalsIgnoreCase(gpsData.getTimestamp()))
			{
				return d;
			}
		}
		
		return null;
	}
	
	public static List<RuntasticData> fromFolder(File exportDataFolder)
	{
		log.info("Creating a Runtastic data list from folder {}", exportDataFolder.getPath());
		
		final List<RuntasticData> runtasticDataList = new LinkedList<>();

		for(File child : exportDataFolder.listFiles())
		{
			if(child.isFile())
			{
				final RuntasticData runtasticData = RuntasticData.fromFile(child);
				if(runtasticData == null)
				{
					log.error("Incorrect Runtastic data found, skipping {}", child.getPath());
				}else
				{
					runtasticData.setFilename(FilenameUtils.getBaseName(child.getPath()));
					runtasticDataList.add(runtasticData);
				}
			}
		}
		
		return runtasticDataList;
	}
	
	public static RuntasticData fromFile(File exportDataFile)
	{

		log.info("Creating a Runtastic data from file {}", exportDataFile.getPath());
		
		File elevationDataFile = new File(FilenameUtils.getFullPath(exportDataFile.getPath()),
				RuntasticData.DEFAULT_ELEVATION_DATA_FOLDER + "/" + FilenameUtils.getName(exportDataFile.getPath()));
		File GPSDataFile = new File(FilenameUtils.getFullPath(exportDataFile.getPath()),
				RuntasticData.DEFAULT_GPS_DATA_FOLDER + "/" + FilenameUtils.getName(exportDataFile.getPath()));

		log.info("Elevation data folder {}", elevationDataFile.getPath());
		log.info("GPS data folder {}", GPSDataFile.getPath());

		ExportData exportData = ExportData.fromFile(exportDataFile);
		ElevationData[] elevationDataArrayTmp = null;
		GPSData[] GPSDataArrayTmp = null;

		elevationDataArrayTmp = ElevationData.fromFile(elevationDataFile);
		GPSDataArrayTmp = GPSData.fromFile(GPSDataFile);

		if(elevationDataArrayTmp == null)
		{
			log.error("Elevation data missing for file {}", exportDataFile.getPath());
		}

		if(GPSDataArrayTmp == null)
		{
			log.error("GPS data missing for file {}", exportDataFile.getPath());
		}

		if(exportData == null)
		{
			log.error("Export data file missing or incorrect {}", exportDataFile.getPath());
		}

		if(elevationDataArrayTmp != null & GPSDataArrayTmp != null && exportData != null)
		{
			return new RuntasticData(exportData, elevationDataArrayTmp, GPSDataArrayTmp);
		}

		return null;
	}
}
