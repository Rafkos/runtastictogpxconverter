package net.rafkos.tools.RuntasticConverter.runtastic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rafkos.tools.RuntasticConverter.Global;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElevationData
{
	private int version;
	private String timestamp;
	private double elevation;
	private int source_type, duration, distance;
	private double elevation_gain, elevation_loss;
	
	public static ElevationData[] fromFile(File file)
	{
		log.info("Creating an elevation data from {}", file.getPath());
		
		FileReader reader = null;
		
		try
		{
			reader = new FileReader(file);
		}catch(FileNotFoundException e)
		{
			log.error("Elevation data not found {}", file.getPath());
			return null;
		}
		ElevationData[] edArray = Global.JSON_PARSER.fromJson(reader, ElevationData[].class);
		
		if(reader != null)
		{
			try
			{
				reader.close();
			}catch(IOException e)
			{
				log.error("An error has occured while closing IO reader {}", e.getLocalizedMessage());
			}
		}
		
		return edArray;
	}
}
// chce kupe
