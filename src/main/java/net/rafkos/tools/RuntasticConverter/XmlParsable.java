package net.rafkos.tools.RuntasticConverter;

public interface XmlParsable
{
	public String toXml();
}
