package net.rafkos.tools.RuntasticConverter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

import lombok.extern.slf4j.Slf4j;
import net.rafkos.tools.RuntasticConverter.endomonto.GPXData;
import net.rafkos.tools.RuntasticConverter.runtastic.RuntasticData;

@Slf4j
public class Converter
{
	public static void convertFromRuntasticToEndomondo(File exportFolder, File destinationFolder)
	{
		if(!destinationFolder.exists() || destinationFolder.isFile())
		{
			log.error("Destination location is not a folder or provided path is incorrect.");
			return;
		}
		
		if(!exportFolder.exists() || exportFolder.isFile())
		{
			log.error("Runtastic export folder is not a folder or provided path is incorrect.");
			return;
		}
		
		log.info("Loading runtastic data from folder {}", exportFolder.getPath());
		List<RuntasticData> runtasticDataList = RuntasticData.fromFolder(exportFolder);
		
		log.info("Starting conversion to GPX...");
		
		for(RuntasticData d : runtasticDataList)
		{
			PrintWriter writer = null;
			try
			{
				File destinationFile = new File(destinationFolder, d.getFilename() + ".gpx");
				
				log.info("Writing new GPX file as {}", destinationFile.getPath());
				
				writer = new PrintWriter(destinationFile, "UTF-8");
				
				GPXData gpxData = GPXData.fromRuntasticData(d);
				
				if(gpxData != null)
				{
					writer.write(gpxData.toXml());
					writer.flush();
				}else
				{
					log.error("Incorrect GPX data, skipping writing to file {}", d.getFilename());
				}
			}catch(FileNotFoundException | UnsupportedEncodingException e)
			{
				log.error(e.getLocalizedMessage());
			}
		
			if(writer != null)
			{
				writer.close();
			}
		}
		
		log.info("Done. Please check the log for errors.");
	}
}
