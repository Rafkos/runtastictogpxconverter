package net.rafkos.tools.RuntasticConverter.endomonto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.rafkos.tools.RuntasticConverter.XmlParsable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TRKPtData implements XmlParsable
{
	private double longitude, latitude, elevation;
	private String time;
	@Override
	public String toXml()
	{
		final StringBuilder builder = new StringBuilder();
		
		builder.append("<trkpt lon=\""+longitude+"\" lat=\""+latitude+"\">\n");
		builder.append("\t<ele>"+elevation+"</ele>\n");
		builder.append("\t<time>"+time+"</time>\n");
		builder.append("</trkpt>\n");
		
		return builder.toString();
	}
}
