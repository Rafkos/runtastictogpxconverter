package net.rafkos.tools.RuntasticConverter.endomonto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.rafkos.tools.RuntasticConverter.XmlParsable;
import net.rafkos.tools.RuntasticConverter.runtastic.ElevationData;
import net.rafkos.tools.RuntasticConverter.runtastic.GPSData;
import net.rafkos.tools.RuntasticConverter.runtastic.RuntasticData;

@Slf4j
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GPXData implements XmlParsable
{
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	private int year;
	private String time;
	private TRKData[] trk;

	public static GPXData fromRuntasticData(RuntasticData runtasticData)
	{
		GPXData gpxData = new GPXData();
		gpxData.setTime(DATE_FORMAT.format(new Date(runtasticData.getExportData().getCreated_at())));
		gpxData.setYear(
				Integer.parseInt((new SimpleDateFormat("yyyy")).format(runtasticData.getExportData().getCreated_at())));

		TRKData trkData = new TRKData();
		gpxData.setTrk(new TRKData[] { trkData });

		List<TRKPtData> trkSegDataList = new LinkedList<>();

		for(ElevationData d : runtasticData.getElevationDataArray())
		{
			GPSData gpsData = runtasticData.getFor(d);

			if(gpsData == null)
			{
				log.error("Unable to find corresponding GPS data for elevation data in file {}",
						runtasticData.getFilename());
			}else
			{
				TRKPtData trkPtData = new TRKPtData();
				trkPtData.setElevation(d.getElevation());
				trkPtData.setLatitude(gpsData.getLatitude());
				trkPtData.setLongitude(gpsData.getLongitude());

				try
				{
					trkPtData.setTime(
							DATE_FORMAT.format(RuntasticData.TIMESTAMP_DATE_FORMAT.parse(gpsData.getTimestamp())));
				}catch(ParseException e)
				{
					log.error("An error has occured during converting timestamp data. Check date formatting. {}",
							e.getLocalizedMessage());
					continue;
				}

				trkSegDataList.add(trkPtData);
			}
		}

		if(trkSegDataList.size() > 0)
		{
			TRKSegData trkSegData = new TRKSegData();
			trkData.setTrkSegData(new TRKSegData[] { trkSegData });

			trkSegData.setTrkPtData(trkSegDataList.toArray(new TRKPtData[trkSegDataList.size()]));
		}else
		{
			log.error("No GPS and elevation data imported. Data missing or errors have occured.");
			return null;
		}

		return gpxData;
	}

	@Override
	public String toXml()
	{
		final StringBuilder builder = new StringBuilder();

		builder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<gpx version=\"1.1\" creator=\"http://rafkos.net\" xsi:schemaLocation=\"http://www.topografix.com/GPX/1/1\n"
				+ "                                http://www.topografix.com/GPX/1/1/gpx.xsd\n"
				+ "                                http://www.garmin.com/xmlschemas/GpxExtensions/v3\n"
				+ "                                http://www.garmin.com/xmlschemas/GpxExtensionsv3.xsd\n"
				+ "                                http://www.garmin.com/xmlschemas/TrackPointExtension/v1\n"
				+ "                                http://www.garmin.com/xmlschemas/TrackPointExtensionv1.xsd\" xmlns=\"http://www.topografix.com/GPX/1/1\" xmlns:gpxtpx=\"http://www.garmin.com/xmlschemas/TrackPointExtension/v1\" xmlns:gpxx=\"http://www.garmin.com/xmlschemas/GpxExtensions/v3\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n"
				+ "<metadata>\n" 
				+ "\t<copyright author=\"http://www.rafkos.net\">\n" 
				+ "\t<year>"+year+"</year>\n" 
				+ "\t</copyright>\n" 
				+ "\t<time>"+time+"</time>\n" 
				+ "</metadata>\n");

		for(TRKData d : trk)
		{
			builder.append(d.toXml());
		}

		builder.append("</gpx>");

		return builder.toString();
	}
}
