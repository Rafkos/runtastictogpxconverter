package net.rafkos.tools.RuntasticConverter.endomonto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.rafkos.tools.RuntasticConverter.XmlParsable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TRKData implements XmlParsable
{
	private TRKSegData[] trkSegData;

	@Override
	public String toXml()
	{
		if(trkSegData == null || trkSegData.length == 0)
		{
			return "";
		}
		
		final StringBuilder builder = new StringBuilder();
		
		builder.append("<trk>\n");
		
		for(TRKSegData d : trkSegData)
		{
			builder.append(d.toXml());
		}
		
		builder.append("</trk>\n");
		
		return builder.toString();
	}
}
