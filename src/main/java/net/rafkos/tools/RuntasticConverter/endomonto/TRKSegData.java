package net.rafkos.tools.RuntasticConverter.endomonto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.rafkos.tools.RuntasticConverter.XmlParsable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TRKSegData implements XmlParsable
{
	private TRKPtData[] trkPtData;

	@Override
	public String toXml()
	{
		if(trkPtData == null || trkPtData.length == 0)
		{
			return "";
		}
		
		final StringBuilder builder = new StringBuilder();
		
		builder.append("<trkseg>\n");
		
		for(TRKPtData d : trkPtData)
		{
			builder.append(d.toXml());
		}
		
		builder.append("</trkseg>\n");
		
		return builder.toString();
	}
}
